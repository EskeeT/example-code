import styled from 'styled-components';
import css from '@styled-system/css';
import { SpaceBox } from '@abit/ui/components/SpaceBox';
import { SCREEN_SECTION_WIDTHS } from 'consts/screen';

export const TooltipContent = styled.div(
  css({
    py: 2,
    width: 160,
  })
);

export const Header = styled.div(
  css({
    display: 'flex',
    justifyContent: 'space-between',
  })
);

export const EmptyBlock = styled.div(
  css({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    pt: 30,
  })
);

export const MenuContainer = styled.div(
  css({
    pl: [6, 8, 10, 10],
    pr: [6, 8, 10, 4],
  })
);

export const Container = styled.div(
  css({
    paddingY: 3,
    width: SCREEN_SECTION_WIDTHS,
    overflowY: 'auto',
    maxHeight: '100%',
    flexShrink: 0,
  })
);

export const Section = styled(SpaceBox)(({ theme }) =>
  css({
    px: 2,
    py: '6px',
    background: 'white',
    borderRadius: 20,
    mx: '-8px',
    boxShadow: `0px 3px 9px -3px ${theme.colors.black15}`,
  })
);
