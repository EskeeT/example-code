import { memo, useCallback, useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useDebounce } from 'components/Tasks/hooks';

import { Box } from '@abit/ui/components/Box';
import { Text } from '@abit/ui/components/Text';
import { Button } from '@abit/ui/components/Button';
import { Dropdown, DropdownItem } from '@abit/ui/components/Dropdown';

import { ListCard } from './ListCard';
import { GroupsAndLists } from './GroupsAngLists/GroupsAndLists';
import { Loader } from 'components/common/Loader';
import { Group as ModalGroup } from './modals/Group';
import { List as ModalList } from './modals/List';

import { IGroupsAndListsData, sortMenu } from 'api/menu';

import { sortGroupsAndLists, sortMenu as funcSortMenu } from '../utils';

import { useAppSelector } from 'store/hooks';
import { useAppDispatch } from 'store/hooks';
import actions from 'store/actions';

import { Header, EmptyBlock, MenuContainer, Container, Section } from './styled';

export const Menu: React.FC = memo(() => {
  const [modeModal, setModeModal] = useState<'newGroup' | 'newList' | null>(null);

  const dispatch = useAppDispatch();

  const loader = useAppSelector((state) => state.app.loader);
  const data = useAppSelector((state) => state.app.data);
  const duplicateData = useAppSelector((state) => state.app.duplicateData);

  const debouncedData = useDebounce<IGroupsAndListsData | null>(data, 1000);

  const firstRender = useRef(true);

  const id = Object.values(useParams())[0];

  let prefix = id as string;

  if (id?.includes('-')) {
    const arrayIds = id.split('-');
    prefix = arrayIds[0];
  }

  const handleSort = useCallback(
    (dragIndex: number, hoverIndex: number, arrFindGroup?: number[]) => {
      if (!data || !arrFindGroup || dragIndex === hoverIndex) return;

      const newData = sortGroupsAndLists(
        data,
        dragIndex,
        hoverIndex,
        arrFindGroup
      ) as unknown as IGroupsAndListsData;

      dispatch(actions.app.sortedMenu(newData));
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [data?.groupsAndLists]
  );

  const sendSortArr = async (
    sortArr: { itemId: string; sort: number; parentId?: string }[],
    newArr: IGroupsAndListsData
  ) => {
    try {
      const response = await sortMenu(sortArr);

      if (response.success) {
        dispatch(actions.app.setNewDuplicateData(newArr));
      }

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (err: any) {
      toast.error(err.toString());
    }
  };

  useEffect(() => {
    if (!duplicateData || !debouncedData) return;

    if (firstRender.current) {
      firstRender.current = false;
      return;
    }

    const backendArr = funcSortMenu(duplicateData.groupsAndLists);
    const newArr = funcSortMenu(debouncedData.groupsAndLists);

    const filteredArr = newArr.filter(
      (item) => !backendArr.find((elem) => elem.itemId === item.itemId && elem.sort === item.sort)
    );

    const addGroupFilteredArr = newArr.filter((item) =>
      filteredArr.find((elem) => elem.parentId === item.parentId)
    );

    sendSortArr(addGroupFilteredArr, debouncedData as unknown as IGroupsAndListsData);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedData]);

  if (loader)
    return (
      <Box width="100%" height="300px">
        <Loader />
      </Box>
    );

  const validData = !data?.groupsAndLists.length && !data?.removedLists.length && !data?.assignedLists.length;

  return (
    <Container>
      {validData ? (
        <EmptyBlock>
          <Text variant="body1Semibold" color="textSecondary">
            Start adding groups or lists
          </Text>
          <Button
            style={{ width: '150px' }}
            mt={6}
            size="s"
            onClick={() => {
              setModeModal('newGroup');
            }}
            variant="primary"
          >
            Add group
          </Button>
          <Button
            style={{ width: '150px' }}
            mt={6}
            size="s"
            onClick={() => {
              setModeModal('newList');
            }}
            variant="primary"
          >
            Add list
          </Button>
        </EmptyBlock>
      ) : (
        <MenuContainer>
          <Header>
            <Text variant="body2Regular" py={2} color="textSecondary">
              YOUR
            </Text>
            <Dropdown $size="xs" label="Create">
              <DropdownItem label="New list" onClick={() => setModeModal('newList')} />
              <DropdownItem label="New group" onClick={() => setModeModal('newGroup')} />
            </Dropdown>
          </Header>
          <Section>
            {data.groupsAndLists.map((groupsAndLists, index) => {
              const arrFindGroup: number[] = [];
              return (
                <Box key={index}>
                  <GroupsAndLists
                    onSort={handleSort}
                    groupOrList={groupsAndLists}
                    indent={0}
                    findGroup={arrFindGroup}
                    index={index}
                    activeList={prefix}
                  />
                </Box>
              );
            })}
          </Section>

          {data.assignedLists.length !== 0 && (
            <>
              <Text mt={6} py={2} variant="body2Regular" color="textSecondary">
                ASSIGNED
              </Text>
              <Section>
                {data.assignedLists.map((list, index) => (
                  <ListCard
                    isActiveList={prefix === list.prefix}
                    sort={list.sort}
                    index={index}
                    editDate={list.updatedAt}
                    createDate={list.createdAt}
                    id={list._id}
                    prefix={list.prefix}
                    key={list._id}
                    name={list.name}
                    color={list.color}
                    type={list.type}
                    indent={0}
                    variant={'assigned'}
                  />
                ))}
              </Section>
            </>
          )}
          {data.removedLists.length !== 0 && (
            <>
              <Text mt={6} py={2} variant="body2Regular" color="textSecondary">
                REMOVED
              </Text>
              <Section>
                {data.removedLists.map((list, index) => (
                  <ListCard
                    isActiveList={prefix === list.prefix}
                    sort={list.sort}
                    index={index}
                    key={list._id}
                    prefix={list.prefix}
                    id={list._id}
                    name={list.name}
                    color={list.color}
                    indent={0}
                    createDate={list.createdAt}
                    editDate={list.updatedAt}
                    type={list.type}
                    variant={'removed'}
                  />
                ))}
              </Section>
            </>
          )}
        </MenuContainer>
      )}

      {modeModal === 'newGroup' && <ModalGroup variant={'new'} onClose={() => setModeModal(null)} />}
      {modeModal === 'newList' && <ModalList variant={'new'} onClose={() => setModeModal(null)} />}
    </Container>
  );
});
